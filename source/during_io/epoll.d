module during_io.epoll;
import during_io.internal;
import core.sys.posix.sys.types;
import core.sys.linux.errno;
import core.atomic;
import std.stdio;
import core.sys.linux.epoll;

struct Epoll
{
    @disable this();
    //@disable this(this);

    static auto init(uint entries)
    {
        import std.typecons : tuple;
        import std.algorithm : move;

        auto q = Epoll(entries);
        auto res = q.setup();
        return ReturnTuple(res, q);
    }

    static struct ReturnTuple
    {
        Epoll q;
        int err;

        alias q this;

        @disable this();
        this(int error, Epoll queue)
        {
            err = error;
            q = queue;
        }

        @property bool isOk()
        {
            return err == 0;
        }

        @property string errorMsg()
        {
            import core.stdc.string : strerror;
            import std.string : fromStringz;

            return strerror(err).fromStringz.idup;
        }
    }

    void exit()
    {
        import core.sys.linux.unistd : close;

        if (fd >= 0)
        {
            close(fd);
        }
    }

    auto waitCQEntry()
    {
        CQEntry cqe;
        if (remainingEvents.length == 0)
        {
            auto nevs = epoll_wait(fd, events.ptr, entries, -1);
            if (nevs != -1)
            {
                remainingEvents = events[0 .. nevs];
            }

        }

        if (remainingEvents.length)
        {
            auto ev = remainingEvents[0];
            cqe.res = ev.events;
            cqe.user_data = ev.data.fd;
        }
        return cqe;
    }

    auto pollAdd(int fd, ushort mask)
    {
        epoll_event ev;
        ev.data.fd = fd;
        ev.events = mask | EPOLLET;
        if (epoll_ctl(this.fd, EPOLL_CTL_ADD, fd, &ev) == -1) {
            perror("epoll_ctl()");
        }
    }


    auto pollRemove(int fd, ushort mask)
    {

    }


    int submit()
    {
        return submit(0);
    }

    void seenCQEntry(CQEntry cqe)
    {
        if (cqe.user_data)
            advanceCQ(1);
    }

    @property bool isOverflow()
    {
        return false;
    }


    int fd;

private:
    this(uint entries)
    {
        this.entries = entries;
        this.events.length = entries;
    }

    auto setup()
    {
        fd = epoll_create1(0);
        int ret;
        if (fd == -1)
            ret = errno;
        return ret;
    }

    public int submit(uint wait_nr)
    {
        return 1;
    }

    void advanceCQ(uint nr)
    {
        if (nr)
        {
            remainingEvents = remainingEvents[nr .. $];
        }
    }

    epoll_event[] events;
    epoll_event[] remainingEvents;
    uint entries;

}
