module during_io.io_uring;
import during_io.internal;
import core.sys.posix.sys.types;
import core.sys.linux.errno;
import core.atomic;

import std.stdio;

struct Ring
{
    @disable this();
    //@disable this(this);

    static auto init(uint entries, RingFlag flags)
    {
        import std.typecons : tuple;
        import std.algorithm : move;

        auto q = Ring(entries, flags);
        auto res = q.setup();
        return ReturnTuple(res, q);
    }

    static struct ReturnTuple
    {
        Ring q;
        int err;

        alias q this;

        @disable this();
        this(int error, Ring queue)
        {
            err = error;
            q = queue;
        }

        @property bool isOk()
        {
            return err == 0;
        }

        @property string errorMsg()
        {
            import core.stdc.string : strerror;
            import std.string : fromStringz;

            return strerror(err).fromStringz.idup;
        }
    }

    void exit()
    {
        import core.sys.linux.sys.mman;
        import core.sys.linux.unistd : close;

        if (fd >= 0)
        {
            munmap(sq.sqes.ptr, *sq.kring_entries * SQEntry.sizeof);
            munmap(sq.ring_ptr, sq.ring_sz);
            munmap(cq.ring_ptr, cq.ring_sz);
            close(fd);
        }
    }

    SQEntry* getSQEntry()
    {
        auto next = sq.sqe_tail + 1;
        SQEntry* sqe = null;

        /*
        * All sqes are used
        */
        if (next - sq.sqe_head > *sq.kring_entries)
            return null;

        sqe = &sq.sqes[sq.sqe_tail & *sq.kring_mask];
        sq.sqe_tail = next;
        return sqe;
    }

    auto waitCQEntry()
    {
        CQEntry* cqe = null;
        auto ret = getCQEntry(cqe, 1);
        if (ret == 0)
        {
            return cqe;
        }
        else
        {
            return null;
        }
    }

    auto prepareReadv(int fd, const void* iovecs, uint nr_vecs, off_t offset)
    {
        return prepareRW(EnterOpcode.readv, fd, iovecs, nr_vecs, offset);
    }

    auto pollAdd(int fd, ushort mask)
    {
        auto sqe = getSQEntry;
        if (sqe)
        {
            *sqe = SQEntry.init;
            sqe.user_data = fd;
            sqe.opcode = EnterOpcode.poll_add;
            sqe.fd = fd;
            sqe.poll_events = mask;
        }

        return sqe;
    }

    auto pollRemove(int fd, ushort mask)
    {
        auto sqe = getSQEntry;
        if (sqe)
        {
            *sqe = SQEntry.init;
            sqe.user_data = fd;
            sqe.opcode = EnterOpcode.poll_remove;
            sqe.fd = fd;
            sqe.poll_events = mask;
        }

        return sqe;
    }

    auto prepareRW(EnterOpcode op, int fd, const void* addr, uint len, off_t offset)
    {
        auto sqe = getSQEntry;
        if (sqe)
        {
            *sqe = SQEntry.init;
            sqe.opcode = op;
            sqe.fd = fd;
            sqe.off = offset;
            sqe.addr = cast(ulong) addr;
            sqe.len = len;
        }

        return sqe;
    }

    int submit()
    {
        return submit(0);
    }

    void seenCQEntry(CQEntry* cqe)
    {
        if (cqe)
            advanceCQ(1);
    }

    @property bool isOverflow()
    {
        return *cq.koverflow > 0;
    }

    RingFlag flags;
    SQ sq;
    CQ cq;
    int fd;

private:
    this(uint entries, RingFlag flags)
    {
        this.flags = flags;
        this.entries = entries;
        p.flags = flags;
    }

    auto setup()
    {
        fd = io_uring_setup(entries, &p);
        int ret;
        if (fd >= 0)
        {
            ret = do_mmap();
            if (ret == 0)
            {
                this.flags = p.flags;
            }
            else
            {
                import core.sys.linux.unistd : close;

                close(fd);
                fd = fd.init;
            }
        }
        else
            ret = errno;
        return ret;
    }

    int do_mmap()
    {
        import core.sys.linux.sys.mman;

        size_t size;

        sq.ring_sz = p.sq_off.array + p.sq_entries * uint.sizeof;
        sq.ring_ptr = mmap(null, sq.ring_sz, PROT_READ | PROT_WRITE,
                MAP_SHARED | MAP_POPULATE, fd, MmapOffset.sq_ring);
        if (sq.ring_ptr == MAP_FAILED)
            return -errno;
        sq.khead = cast(uint*)(sq.ring_ptr + p.sq_off.head);
        sq.ktail = cast(uint*)(sq.ring_ptr + p.sq_off.tail);
        sq.kring_mask = cast(uint*)(sq.ring_ptr + p.sq_off.ring_mask);
        sq.kring_entries = cast(uint*)(sq.ring_ptr + p.sq_off.ring_entries);
        sq.kflags = cast(SQRingFlag*)(sq.ring_ptr + p.sq_off.flags);
        sq.kdropped = cast(uint*)(sq.ring_ptr + p.sq_off.dropped);
        sq.array = cast(uint*)(sq.ring_ptr + p.sq_off.array);

        size = p.sq_entries * SQEntry.sizeof;
        void* sqes = mmap(null, size, PROT_READ | PROT_WRITE,
                MAP_SHARED | MAP_POPULATE, fd, MmapOffset.sqes);
        if (sqes == MAP_FAILED)
        {
            munmap(sq.ring_ptr, sq.ring_sz);
            return errno;
        }
        sq.sqes = (cast(SQEntry*) sqes)[0 .. p.sq_entries];
        cq.ring_sz = p.cq_off.cqes + p.cq_entries * CQEntry.sizeof;
        cq.ring_ptr = mmap(null, cq.ring_sz, PROT_READ | PROT_WRITE,
                MAP_SHARED | MAP_POPULATE, fd, MmapOffset.cq_ring);
        if (cq.ring_ptr == MAP_FAILED)
        {
            munmap(sqes, *sq.kring_entries * SQEntry.sizeof);
            munmap(sq.ring_ptr, sq.ring_sz);
            return errno;
        }
        cq.khead = cast(uint*)(cq.ring_ptr + p.cq_off.head);
        cq.ktail = cast(uint*)(cq.ring_ptr + p.cq_off.tail);
        cq.kring_mask = cast(uint*)(cq.ring_ptr + p.cq_off.ring_mask);
        cq.kring_entries = cast(uint*)(cq.ring_ptr + p.cq_off.ring_entries);
        cq.koverflow = cast(uint*)(cq.ring_ptr + p.cq_off.overflow);
        cq.cqes = (cast(CQEntry*)(cq.ring_ptr + p.cq_off.cqes))[0 .. p.cq_entries];
        return 0;
    }

    public int submit(uint wait_nr)
    {
        const mask = *sq.kring_mask;
        uint ktail = void;
        uint submitted;
        uint to_submit = void;
        EnterFlag flags;
        int ret = void;

        if (sq.sqe_head == sq.sqe_tail)
            return 0;

        /*
        * Fill in sqes that we have queued up, adding them to the kernel ring
        */

        ktail = *sq.ktail;
        to_submit = sq.sqe_tail - sq.sqe_head;
        while (to_submit--)
        {
            sq.array[ktail & mask] = sq.sqe_head & mask;
            ktail++;
            sq.sqe_head++;
            submitted++;
        }

        if (!submitted)
            return 0;

        /*
        * Ensure that the kernel sees the SQE updates before it sees the tail
        * update.
        */
        atomicStore(sq.ktail.ptos, ktail);

        if (wait_nr || needsEnter(flags))
        {
            if (wait_nr)
            {
                import std.algorithm : min;
                flags |= EnterFlag.getevents;
                wait_nr = min(submitted, wait_nr);
            }

            ret = io_uring_enter(fd, submitted, wait_nr, flags, null);
            if (ret < 0)
                return -errno;
        }
        else
        {
            ret = submitted;
        }

        return ret;
    }

    bool needsEnter(ref EnterFlag flags)
    {
        if (!(this.flags & RingFlag.sqpoll))
            return true;
        if ((*sq.kflags & SQRingFlag.need_wakeup))
        {
            flags |= EnterFlag.sq_wakeup;
            return true;
        }
        return false;
    }

    auto getCQEntry(ref CQEntry* cqe, int wait)
    {
        uint head = void;
        int ret = void;

        do
        {
            for (head = *cq.khead; (cqe = (head != atomicLoad(cq.ktail.ptos)
                    ? &cq.cqes[head & (*cq.kring_mask)] : null)) != null; head++)
                break;
            if (cqe)
                break;
            if (!wait)
                break;
            ret = io_uring_enter(fd, 0, 1, EnterFlag.getevents, null);
            if (ret < 0)
                return -errno;
        }
        while (1);

        return 0;
    }

    void advanceCQ(uint nr)
    {
        if (nr)
        {
            atomicStore(cq.khead.ptos, *cq.khead + nr);
        }
    }

    SetupParams p;
    uint entries;

    // static struct Ring
    // {
    //     SQ sq;
    //     CQ cq;
    //     RingFlag flags;
    //     int ring_fd;
    // }

    static struct SQ
    {
        uint* khead;
        uint* ktail;
        uint* kring_mask;
        uint* kring_entries;
        SQRingFlag* kflags;
        uint* kdropped;
        uint* array;
        SQEntry[] sqes;

        uint sqe_head;
        uint sqe_tail;

        size_t ring_sz;
        void* ring_ptr;
    }

    static struct CQ
    {
        uint* khead;
        uint* ktail;
        uint* kring_mask;
        uint* kring_entries;
        uint* koverflow;
        CQEntry[] cqes;

        size_t ring_sz;
        void* ring_ptr;
    }

}
