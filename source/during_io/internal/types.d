module during_io.internal.types;

import core.sys.posix.poll;

enum PollMask: short
{
	pollin = POLLIN,
	pollout = POLLOUT,
	pollhup = POLLHUP,
	pollerr = POLLERR,
}

enum SQEventFlag : ubyte
{
	none       = 0x0,
    fixed_file = 0x01,
    io_drain   = 0x02,
    io_link    = 0x04,
}

enum RingFlag : uint
{
	none = 0x00,
    iopoll = 0x01,
    sqpoll = 0x02,
    sq_aff = 0x04,
}

enum EnterOpcode : ubyte
{

    nop = 0,
    readv =	1,
    writev = 2,
    fsync = 3,
    read_fixed = 4,
    write_fixed = 5,
    poll_add = 6,
    poll_remove = 7,
    sync_file_range = 8,
    sendmsg = 9,
    recvmsg = 10,
}

enum FsyncFlag : uint
{
	none = 0x0,
    datasync = 0x01,
}


enum MmapOffset : ulong
{
    sq_ring = 0x00000000,
    cq_ring = 0x08000000,
    sqes    = 0x10000000,
}


enum RegisterOpcode : uint
{
    register_buffers   = 0x00,
    unregister_buffers = 0x01,
    register_files     = 0x02,
    unregister_files   = 0x03,
    register_eventfd   = 0x04,
}

enum SQRingFlag : uint
{
   need_wakeup = 0x01,
}

enum EnterFlag : uint
{
	none = 0x00,
    getevents = 0x01,
    sq_wakeup = 0x02,
}

/*
 * IO submission data structure (Submission Queue Entry)
 */
struct SQEntry {
	EnterOpcode	opcode;		/* type of operation for this sqe */
	SQEventFlag	flags;		/* IOSQE_ flags */
	ushort	ioprio;		/* ioprio for the request */
	int 	fd;		    /* file descriptor to do IO on */
	ulong	off;		/* offset into file */
	ulong	addr;		/* pointer to buffer or iovecs */
	uint	len;		/* buffer size or number of iovecs */
	union {
		int	        rw_flags;
		FsyncFlag		fsync_flags;
		ushort		poll_events;
		uint		sync_range_flags;
		uint		msg_flags;
	}
	ulong	user_data;	/* data to be passed back at completion time */
	union {
		ushort	    buf_index;	/* index into fixed buffers, if used */
		ulong[3]	__pad2;
	}
}



/*
 * IO completion data structure (Completion Queue Entry)
 */
struct CQEntry {
	ulong	user_data;	/* sqe->data submission passed back */
	int	    res;		/* result code for this event */
	uint	flags;
}


/*
 * Filled with the offset for mmap(2)
 */
struct SQRingOffsets {
	uint  head;
	uint  tail;
	uint  ring_mask;
	uint  ring_entries;
	SQRingFlag  flags;
	uint  dropped;
	uint  array;
	uint  resv1;
	ulong resv2;
}



struct CQRingOffsets {
	uint        head;
	uint        tail;
	uint        ring_mask;
	uint        ring_entries;
	uint        overflow;
	uint        cqes;
	ulong[2]    resv;
}

/*
 * Passed in for io_uring_setup(2). Copied back with updated info on success
 */
struct SetupParams {
	uint sq_entries;
	uint cq_entries;
	RingFlag flags;
	uint sq_thread_cpu;
	uint sq_thread_idle;
	uint[5] resv;
	SQRingOffsets sq_off;
	CQRingOffsets cq_off;
}

auto ref ptos(T)(T* v)
{
	return *cast(shared T*)v;
}
