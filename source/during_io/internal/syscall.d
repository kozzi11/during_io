module during_io.internal.syscall;

import during_io.internal.types;
import core.sys.posix.signal : sigset_t;
import core.stdc.config : c_long;

version(linux):
extern (C) nothrow @nogc c_long syscall(c_long number, ...);

version(Alpha)
{
    enum Syscall : uint {
        SYS_io_uring_setup	= 535,
        SYS_io_uring_enter = 536,
        SYS_io_uring_register = 537,
    }
}
else
{
    enum Syscall : uint {
        SYS_io_uring_setup = 425,
        SYS_io_uring_enter = 426,
        SYS_io_uring_register = 427,
    }
}

int io_uring_register(int fd, RegisterOpcode opcode, const void *arg, uint nr_args)
{
	return cast(int)syscall(Syscall.SYS_io_uring_register, fd, opcode, arg, nr_args);
}

int io_uring_setup(uint entries, SetupParams *p)
{
	return cast(int)syscall(Syscall.SYS_io_uring_setup, entries, p);
}

int io_uring_enter(int fd, uint to_submit, uint min_complete, EnterFlag flags, sigset_t *sig)
{
	return cast(int)syscall(Syscall.SYS_io_uring_enter, fd, to_submit, min_complete,
			flags, sig);
}