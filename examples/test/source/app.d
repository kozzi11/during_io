import std.stdio;
import std.conv;
import std.exception;
import std.string : toStringz;

import during_io.io_uring;
import during_io.internal.types;

import core.sys.posix.sys.uio;
import core.sys.posix.unistd;
import core.sys.linux.fcntl;
import core.sys.linux.errno;
import core.sys.posix.stdlib : posix_memalign;
import core.stdc.string;

enum O_DIRECT = 0x40000;
enum QD = 4;

int main(string[] args)
{
    auto ring = Ring.init(QD, RingFlag.none);
	int i, fd, ret, pending, done;
	SQEntry *sqe;
	CQEntry *cqe;
	iovec[QD] iovecs;
	off_t offset;
	void *buf;

	if (args.length < 2) {
		writef("%s: file\n", args[0]);
		return 1;
	}

	if (!ring.isOk) {
		writeln(ring.errorMsg);
		return 1;
	}

	fd = open(args[1].toStringz, O_RDONLY | O_DIRECT);
	if (fd < 0) {
	 	perror("open");
	 	return 1;
	}

	for (i = 0; i < QD; i++) {
		if (posix_memalign(&buf, 4096, 4096))
			return 1;
		iovecs[i].iov_base = buf;
		iovecs[i].iov_len = 4096;
	}

	offset = 0;
	i = 0;
	do {
		if (i == QD)
			break;
		sqe = ring.prepareReadv(fd, &iovecs[i], 1, offset);
		if (!sqe)
			break;
		offset += iovecs[i].iov_len;
		i++;
	} while (1);

	ret = ring.submit();
	if (ret < 0) {
		stderr.writefln("io_uring_submit: %s\n", strerror(-ret));
		return 1;
	}

	done = 0;
	pending = ret;
	for (i = 0; i < pending; i++) {
		cqe = ring.waitCQEntry();
		if (cqe is null) {
			stderr.writefln("io_uring_wait_cqe: %s\n", strerror(errno));
			return 1;
		}

		done++;
		ret = 0;
		if (cqe.res != 4096) {
			stderr.writefln("ret=%d, wanted 4096\n", cqe.res);
			ret = 1;
		}
		ring.seenCQEntry(cqe);
		if (ret)
			break;
	}

	writefln("Submitted=%d, completed=%d\n", pending, done);
	close(fd);
	ring.exit();

	return 0;

}