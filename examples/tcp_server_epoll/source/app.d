import std.stdio : writeln, writefln, stderr, perror;
import std.conv;
import std.exception;
import std.string : toStringz;

import during_io.epoll;
import during_io.internal.types : CQEntry, PollMask;

import core.sys.posix.sys.uio;
import core.sys.posix.sys.socket;
import core.sys.posix.unistd;
import core.sys.linux.fcntl;
import core.sys.linux.sys.socket;
import core.sys.linux.errno;
import core.sys.posix.stdlib : posix_memalign;
import core.stdc.string;
import core.stdc.stdio;
import core.sys.posix.netinet.in_;
import core.sys.posix.netinet.tcp;

extern (C) int accept4(int sockfd, sockaddr *addr, socklen_t *addrlen, int flags);

static if (!is(typeof(SOCK_NONBLOCK)))
	enum SOCK_NONBLOCK = 0x800;


enum O_DIRECT = 0x40000;
enum QD = 1024;
enum PORT = 8080;
string response = "HTTP/1.1 200 OK\r\nServer: v.d/1.7\r\nDate: Mon, 08 Jul 2019 17:39:14 GMT\r\nContent-Type: text/plain\r\nContent-Length: 13\r\n\r\nHello, World!";
static char[1024] buf2;

bool[int] clientsMap;

static void set_nonblocking(int fd)
{
	int flags = fcntl(fd, F_GETFL, 0);
	if (flags == -1)
	{
		perror("fcntl()");
		return;
	}
	if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1)
	{
		perror("fcntl()");
	}
}

int main(string[] args)
{
	import std.parallelism : totalCPUs;
	import std.concurrency : spawn;
	import std.functional : toDelegate;

	for(int i = totalCPUs - 1; i; i--)
	{
		auto t = spawn((){server;});
	}

	return server();
}

static int server()
{
	auto ring = Epoll.init(QD);
	int i, fd, ret, pending, done;


	off_t offset;
	void* buf;

	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1)
	{
		perror("socket()");
		return 1;
	}
	int enable = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &enable, enable.sizeof) == -1)
	{
		perror("setsockopt()");
		return 1;
	}

	// bind
	sockaddr_in addr;
	memset(&addr, 0, addr.sizeof);
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	addr.sin_port = htons(PORT);
	if (bind(sock, cast(sockaddr*)&addr, addr.sizeof) < 0)
	{
		perror("bind()");
		return 1;
	}

	// make it nonblocking, and then listen
	set_nonblocking(sock);
	if (listen(sock, SOMAXCONN) < 0)
	{
		perror("listen()");
		return 1;
	}

	if (!ring.isOk)
	{
		writeln(ring.errorMsg);
		return 1;
	}

	ring.pollAdd(sock, PollMask.pollin | PollMask.pollout);
	CQEntry cqe;

	while ((cqe = ring.waitCQEntry()).user_data)
	{
		bool ringOverflow = ring.isOverflow;
		int need_resubmit = 0;
		short resubmit_mask = 0;
		auto cqe_flags = cqe.flags;
		auto cqe_user_data = cqe.user_data;
		auto cqe_res = cqe.res;
		ring.seenCQEntry(cqe);
		if (ringOverflow) break;

		if (cqe_res & (PollMask.pollhup | PollMask.pollerr))
		{
			continue;
		}

		// accepting
		if (cqe_user_data == sock)
		{
			//ring.pollAdd(sock, PollMask.pollin);
			for (;;)
			{
				sockaddr in_addr;
				socklen_t in_addr_len = in_addr.sizeof;
				int client = accept4(sock, &in_addr, &in_addr_len, SOCK_NONBLOCK);
				if (client == -1)
				{
					if (errno == EAGAIN || errno == EWOULDBLOCK)
					{
						// we processed all of the connections
						break;
					}
					else
					{
						perror("accept()");
						return 1;
					}
				}
				else
				{
					ring.pollAdd(client, PollMask.pollin);
				}
			}
		}
		else
		{
			int client = cast(int) cqe_user_data;
			bool addToQ = false;
			resubmit_mask = PollMask.pollin;
			if (cqe_res & PollMask.pollin)
			{
				for (;;)
				{
					auto nbytes = read(client, buf2.ptr, 1024);
					if (nbytes == -1)
					{
						if (errno == EAGAIN || errno == EWOULDBLOCK)
						{
							break;
						}
						else
						{
							addToQ = false;
							close(client);
						}
					}
					else if (nbytes == 0)
					{
						 addToQ = false;
						break;
					}
					else
					{

						nbytes = write(client, response.ptr, response.length);
						if (nbytes == -1)
						{
							if (errno == EAGAIN || errno == EWOULDBLOCK)
							{
								resubmit_mask = PollMask.pollout;
							}
						}

					}
				}

			}
			else if (cqe_res & PollMask.pollout)
			{
				resubmit_mask = PollMask.pollin;
				auto nbytes = write(client, response.ptr, response.length);
				if (nbytes == -1)
				{
					perror("read()");
					return 1;
				}
				else if (nbytes == 0)
				{
					addToQ = false;
					close(client);
				}

			}
			if (addToQ)
			{
				ring.pollAdd(client, resubmit_mask);
			}
		}

	}

	close(sock);
	ring.exit();

	return 0;

}
